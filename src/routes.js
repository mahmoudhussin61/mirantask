import React from 'react';
import { Route, Switch } from 'react-router';
import Countries from './pages/countries'

import CountriesWithForm from './pages/CoutriesWithForm';
import AddCountryForm from './pages/CoutriesWithForm/AddCountry';
import EditCountryForm from './pages/CoutriesWithForm/EditCountry';


// Application Routes
const Routes = (
    <Switch>

        {/* Home Page */}
        <Route path="/" exact component={Countries} />

        {/* form */}

        <Route path="/country-form" exact component={CountriesWithForm} />
        <Route path="/add-country" exact component={AddCountryForm} />
        <Route path="/edit-country" exact component={EditCountryForm} />

    </Switch>
);

export default Routes;