import { fork, all } from "redux-saga/effects";
import countrySaga from '../../pages/countries/modules/saga'
import countryFormSaga from '../../pages/CoutriesWithForm/modules/saga'

export default function* rootSaga() {
    yield all([fork(countrySaga)]);
    yield all([fork(countryFormSaga)]);
}