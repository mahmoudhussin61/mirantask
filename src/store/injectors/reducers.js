import { combineReducers } from "redux";
import countryReducer from '../../pages/countries/modules/reducer'
import countryFormReducer from '../../pages/CoutriesWithForm/modules/reducer'

export default combineReducers({
  countryReducer,
  countryFormReducer
})