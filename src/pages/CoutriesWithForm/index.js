import { Table , Button } from 'antd'
import Styles from './countries.module.css'
import { useDispatch, useSelector } from 'react-redux'
import columns from './partials/columns'
import { useEffect } from 'react'
import { useHistory } from 'react-router'


export default function Countries(){

    const { loading , dataSource } = useSelector(state => state.countryFormReducer)
    const dispatch = useDispatch()
    const history = useHistory()


    useEffect(() => {
        dispatch({
            type: "GET_COUNTRIES_FORM"
        })
    }, [])

    const navigateEditForm = rec => () => {
        dispatch({
            type: "GET_SELECTED_fOR_EDIT",
            selectedRow:rec
        })
        history.push("/edit-country")
    }


    return (
        <div className={Styles.content}>
            <div className={Styles.container}>
                <div className={Styles.header}>
                    <h4>Countries</h4>
                    <div className="site-button-ghost-wrapper">
                        <Button type="link" onClick={() => history.push("/add-country")}>New Form</Button>
                    </div>
                </div>
            
                <Table
                    rowKey={"id"}
                    bordered
                    pagination={
                        {
                            pageSize:25,
                            showSizeChanger: true,
                            pageSizeOptions: ["5", "10", "20", "50", "100"]
                        }
                    }
                    columns={columns(navigateEditForm)}
                    dataSource={dataSource}
                    loading={loading}
                    // rowClassName={rec => rec["name"] === selectedRow ? Styles.selectedRow : ""}
                >
                    
                </Table>
            </div>
        </div>
    )
}