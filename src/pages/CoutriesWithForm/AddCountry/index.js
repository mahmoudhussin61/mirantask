import { Button, Input , notification } from "antd"
import GoogleMapReact from 'google-map-react';
import { useDispatch, useSelector } from "react-redux"
import Styles from '../countries.module.css'

export default function AddCountry(){

     const form = useSelector(state => state.countryFormReducer.form)
     const dispatch = useDispatch()

     const handleChange = name => ({ target : { value }}) => {
         dispatch({
                type:"HANDLE_FORM_CHANGE",
                payload : { name , value }
            })
    }

    const handleImageChange = e => {
        console.log(e.target.files[0])
        dispatch({
            type:"HANDLE_FORM_CHANGE",
            payload : { name:"flag" , value: e.target.files[0] }
        })
    }

    const submit = () => {
        dispatch({
            type: "ADD_COUNTRY_FORM"
        })
        notification.open({
            message: 'There is an Error',
            description: "404 Api POST/PUT Not Found"
        })
    }

    return <div className={Styles.content}>
                <div className={Styles.container}>
                    <div className={Styles.form}>
                        <div className={Styles.inputContent}>
                            <h2>Add New Country</h2>
                        </div>
                        <div className={Styles.inputContent}>
                            <div className={Styles.InputContainer} >
                                <Input 
                                    value={form.name}
                                    onChange={handleChange("name")}
                                    placeholder="enter name"
                                    className={Styles.Input}
                                />
                            </div>
                            <div className={Styles.InputContainer}>
                                <Input 
                                    value={form.capital}
                                    onChange={handleChange("capital")}
                                    placeholder="enter capital"
                                    className={Styles.Input}
                                />
                            </div>
                        </div>

                        <div className={Styles.inputContent}>
                            <div className={Styles.InputContainer}>
                                <Input 
                                    value={form.population}
                                    onChange={handleChange("population")}
                                    placeholder="enter population"
                                    className={Styles.Input}
                                />
                            </div>
                            <div className={Styles.InputContainer}>
                                <Input 
                                    value={form.region}
                                    onChange={handleChange("region")}
                                    placeholder="enter region"
                                    className={Styles.Input}
                                />
                            </div>
                        </div>

                        <div className={Styles.inputContent}>
                            <div className={Styles.InputContainer}>
                                <Input 
                                    type="file"
                                    className={Styles.Input}
                                    onChange={handleImageChange}
                                />
                            </div>
                            <div className={Styles.InputContainer}>
                                {/* <Input 
                                    value={form.latlng}
                                    onChange={handleChange("latlng")}
                                    placeholder="enter latlng"
                                    className={Styles.Input}
                                /> */}
                                <div style={{ height: '300px', width: '100%' }}>
                                    <GoogleMapReact
                                        // bootstrapURLKeys={{ key: /* YOUR KEY HERE */ }}
                                        defaultCenter={{
                                            lat: 59.95,
                                            lng: 30.33
                                        }}
                                        defaultZoom={11}
                                    >
                                        {() => <div>text</div>}
                                    </GoogleMapReact>
                                </div>
                            </div>
                        </div>
                        
                        <div className={Styles.inputContent}>
                            <Button type="primary" size="middle" onClick={submit}>Submit</Button>
                        </div>
                    </div>
                </div>
            </div>
}