import { Button, Image } from "antd"

export default navigateEditForm => [
    {
        title: 'ID',
        dataIndex: 'id',
        align: "center",
    },
    {
        title: 'Name',
        dataIndex: 'name',
        align: "center",
    },
    {
        title: 'Capital',
        align: "center",
        dataIndex:"capital"
    },
    {
        title: 'Flag',
        align: "center",
        render: rec => {
                return <Image src={rec["flag"]} height={40} width={55} />
            }
    },
    {
        title: 'Region',
        align: "center",
        dataIndex:"region"
    },
    {
        title: 'Population',
        align: "center",
        dataIndex:"population"
    },
    {
        title: 'Position',
        align: "center",
        render : rec => <a href={`https://www.google.com/maps/place/@${rec["latlng"][0]},${rec["latlng"][1]},15z`}>map</a>
    },
    {
        title: 'Actions',
        align: "center",
        render : rec => <div className="site-button-ghost-wrapper">
            {<Button type="ghost" onClick={navigateEditForm(rec)}>Edit</Button>}
        </div>
    },

]

export const initialRowData = {
    name:"",
    capital:"",
    latlng:"",
    population:"",
    region:"",
    flag:""
}