import {
  GET_COUNTRIES_FORM,
  GET_COUNTRIES_FORM_FINISHED,

  ////////
  ADD_COUNTRY_FORM,
  ADD_COUNTRY_FORM_FINISHED,
  EDIT_COUNTRY_FORM,
  EDIT_COUNTRY_FORM_FINISHED,
  HANDLE_FORM_CHANGE,
  GET_SELECTED_fOR_EDIT
} from './type'

export const getCountries = () => ({
    type:GET_COUNTRIES_FORM,
})

export const getCountriesFinished = data => ({
    type:GET_COUNTRIES_FORM_FINISHED,
    payload:data
})


///////////////////////////////////
export const addCountryForm = () => ({
  type:ADD_COUNTRY_FORM
})

export const addCountryFormFinished = () => ({
  type:ADD_COUNTRY_FORM_FINISHED
})

export const handleFormChange = (name,value) => ({
  type:HANDLE_FORM_CHANGE,
  payload:{ name , value }
})

export const getSelectedRecordForEdit = selectedRow => ({
  type: GET_SELECTED_fOR_EDIT,
  selectedRow
})

export const editCountryForm = () => ({
  type:EDIT_COUNTRY_FORM
})

export const editCountryFormFinished = () => ({
  type:EDIT_COUNTRY_FORM_FINISHED
})