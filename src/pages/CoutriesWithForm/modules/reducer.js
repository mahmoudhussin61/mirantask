import {
   GET_COUNTRIES_FORM,
   GET_COUNTRIES_FORM_FINISHED,
   HANDLE_FORM_CHANGE,
   GET_SELECTED_fOR_EDIT
  } from "./type";
  
  
  const initState = {
    loading: false,
    dataSource: [],
    form:{},
  };
  
  export default (state = initState, action) => {
    switch (action.type) {
      case GET_COUNTRIES_FORM:
        return {
          ...state,
          loading: true
        };
  
      case GET_COUNTRIES_FORM_FINISHED:
        return {
          ...state,
          dataSource: action.payload,
          loading: false
        };
  

        case HANDLE_FORM_CHANGE:
          const { name :formName , value : formValue } = action.payload
          return {
            ...state,
            form:{
              ...state.form,
              [formName]: formValue
            }
          }

          case GET_SELECTED_fOR_EDIT: 
            return {
              ...state,
              form:action.selectedRow
            }
  
      
      default:
        return state;
    }
  };
  