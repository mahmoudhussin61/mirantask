import { takeLatest, put, all, select , call } from "redux-saga/effects";
import {
    getCountriesFinished,
    addCountryFormFinished,
    editCountryFormFinished
} from "./action";
import {
    GET_COUNTRIES_FORM,
    ADD_COUNTRY_FORM,
    EDIT_COUNTRY_FORM,
} from "./type";
import axios from 'axios'
import normalize from "./normalize";


function* requestCountries() {
  try {
    let response = yield call(() => axios.get("https://restcountries.eu/rest/v2/all"))
    yield put(getCountriesFinished(normalize(response.data)))
  } catch (error) {
    console.log(error.response);
  }
}


/// for form Editing
function* requestAddCountryForm(){
    try {
        const { form } = yield select( ({ countryFormReducer }) => countryFormReducer)
        const formData = new FormData()
        Object.keys(form).forEach(key => formData.append(key , form[key]))
        let response = yield call(() => axios.post("https://restcountries.eu/rest/v2/all" , formData , {
          headers: {"content-type": "multipart/form-data"}
        }))
        // yield put(addCountryFormFinished(response))
    } catch (error) {
        console.log("error while get ds =>", error.response);
    }
}

function* requestUpdateCountryForm(){
  try {
        const { form } = yield select( ({ countryFormReducer }) => countryFormReducer)
        const formData = new FormData()
        Object.keys(form).forEach(key => formData.append(key , form[key]))
        let response = yield call(() => axios.put(`https://restcountries.eu/rest/v2/all/${form.id}` , formData , {
          headers: {"content-type": "multipart/form-data"}
        }))
        console.log(response)
        // yield put(editCountryFormFinished(response))
  } catch (error) {
      console.log("error while get d =>", error);
  }
}

export default function* countryFormSaga() {
  yield all([takeLatest(GET_COUNTRIES_FORM, requestCountries)]);

  //////
  /// for form Editing
  yield all([takeLatest(ADD_COUNTRY_FORM, requestAddCountryForm)]);
  yield all([takeLatest(EDIT_COUNTRY_FORM, requestUpdateCountryForm)]);
}
