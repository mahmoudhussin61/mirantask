import { Table , Button, notification } from 'antd'
import Styles from './countries.module.css'
import { useDispatch, useSelector } from 'react-redux'
import columns from './modules/columns'
import { useEffect } from 'react'
import { useHistory } from 'react-router'


export default function Countries(){

    const { 
        loading,
        dataSource,
        isEditing,
        selectedRow,
        updatedRow
    } = useSelector(state => state.countryReducer)
    const dispatch = useDispatch()
    const history = useHistory()

    const onChangeInput = name => ({ target }) => {
        dispatch({
            type: "HANDLE_INPUT_CHANGE",
            payload : { name , value : name === "flag" ? target.files[0] : target.value }
        })
    }

    const onRowSelect = selectedRow => () => {
        dispatch({
            type: "EDIT_COUNTRY",
            selectedRow
        })
    }

    const addNewCountry = () => {
        dispatch({
            type:"ADD_NEW_COUNTRY",
        })
    }

    const editRecord = () => {
        dispatch({
            type:updatedRow.hasOwnProperty("isNew") ? "ADD_NEW_RECORD" : "EDIT_RECORD"
        })
        dispatch({
            type:updatedRow.hasOwnProperty("isNew") ? "ADD_NEW_RECORD_FINISHED" : "EDIT_COUNTRY_FINISHED",
            newState: {
                isEditing : false
            }
        })
        notification.open({
            message: 'There is an Error',
            description: "404 Api POST/PUT Not Found"
        })
    }

    useEffect(() => {
        dispatch({
            type: "GET_COUNTRIES"
        })
    }, [])


    return (
        <div className={Styles.content}>
            <div className={Styles.container}>
                <div className={Styles.header}>
                    <h4>Countries</h4>
                    <div className="site-button-ghost-wrapper">
                        <Button type="primary" onClick={addNewCountry}>add new record</Button>
                        <Button type="dashed" onClick={() => history.push("/country-form")}>anthor Way with form</Button>
                    </div>
                </div>
            
                <Table
                    rowKey={"id"}
                    bordered
                    pagination={
                        {
                            pageSize:25,
                            showSizeChanger: true,
                            pageSizeOptions: ["5", "10", "20", "50", "100"]
                        }
                    }
                    columns={columns({
                        isEditing,
                        onChangeInput,
                        onRowSelect,
                        selectedRow,
                        editRecord,
                    })}
                    dataSource={dataSource}
                    loading={loading}
                    // rowClassName={rec => rec["name"] === selectedRow ? Styles.selectedRow : ""}
                >
                    
                </Table>
            </div>
        </div>
    )
}