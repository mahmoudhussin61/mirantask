import {
   GET_COUNTRIES,
   GET_COUNTRIES_FINISHED,
   ADD_NEW_COUNTRY,
   HANDLE_INPUT_CHANGE,
   EDIT_COUNTRY,
   ADD_NEW_RECORD,
   EDIT_RECORD,
   ADD_NEW_RECORD_FINISHED,
   EDIT_COUNTRY_FINISHED
  } from "./type";
  
  import { initialRowData } from "./columns";
  
  const initState = {
    loading: false,
    selectedRow: '',
    isEditing: false,
    updatedRow:{},
    dataSource: [],
  };
  
  export default (state = initState, action) => {
    switch (action.type) {
      case GET_COUNTRIES:
        return {
          ...state,
          loading: true
        };
  
      case GET_COUNTRIES_FINISHED:
        return {
          ...state,
          dataSource: action.payload,
          loading: false
        };
  
      case ADD_NEW_COUNTRY:
        const ds = state.dataSource;
        const key = `${ds.length + 1 + new Date().getDay()}`;
        return {
          ...state,
          dataSource: [
            {
              id: key,
              ...initialRowData
            },
            ...ds
          ],
          isEditing: true,
          selectedRow: key
        };
  
      case EDIT_COUNTRY:
        return {
          ...state,
          isEditing: true,
          selectedRow: action.selectedRow,
        };

      case HANDLE_INPUT_CHANGE:
        const { name ,value } = action.payload
        const currentData = state.dataSource
        const currentIndex = currentData.findIndex(({ id }) => id === state.selectedRow)
        const updatedRow = {...currentData[currentIndex] , [name]: value}
        currentData[currentIndex] = updatedRow
        const updatedDataSource = [...currentData ]

        return {
          ...state,
          dataSource : updatedDataSource,
          updatedRow
          // isEditing: true,
          // selectedRow: action.selectedRow,
        };

        case ADD_NEW_RECORD , EDIT_RECORD: 
          return {
            ...state,
            isEditing:false,
            selectedRow:""
          }

        case ADD_NEW_RECORD_FINISHED:
        case EDIT_COUNTRY_FINISHED : 
            return{
              ...state,
              ...action.newState
            }
      default:
        return state;
    }
  };
  