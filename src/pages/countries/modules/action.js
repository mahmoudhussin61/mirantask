import {
  ADD_NEW_COUNTRY,
  ADD_NEW_COUNTRY_FINISHED,
  EDIT_COUNTRY,
  EDIT_COUNTRY_FINISHED,
  GET_COUNTRIES,
  GET_COUNTRIES_FINISHED,
  HANDLE_INPUT_CHANGE,
  EDIT_RECORD,
  ADD_NEW_RECORD,
  ADD_NEW_RECORD_FINISHED,

} from './type'

export const getCountries = () => ({
    type:GET_COUNTRIES,
})

export const getCountriesFinished = data => ({
    type:GET_COUNTRIES_FINISHED,
    payload:data
})

export const addNewCountry = () => ({
    type:ADD_NEW_COUNTRY,
})

export const editCountry = selectedRow => ({
    type:EDIT_COUNTRY,
    selectedRow
})

export const handleInputChange = (name , value) => ({
    type:HANDLE_INPUT_CHANGE,
    payload : { name , value }
})


export const editRecord = () => ({
  type:EDIT_RECORD
})

export const editRecordFinished = newState => ({
  type:EDIT_COUNTRY_FINISHED,
  newState
})


export const addRecord = () => ({
  type:ADD_NEW_RECORD
})

export const addNewRecordFinished = newState => ({
  type:ADD_NEW_RECORD_FINISHED,
  newState
})