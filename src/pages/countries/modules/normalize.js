export default data => data.map((country, index) => ({
    id:`${country.name}-${index}`,
    ...country
}))