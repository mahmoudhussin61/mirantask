import { Button, Image, Input } from "antd"
import GoogleMapImg from '../../../assets/google-maps.png'

export default ({
    isEditing,
    onChangeInput,
    onRowSelect,
    selectedRow,
    editRecord,
    navigateEditForm
}) => [
    {
        title: 'ID',
        dataIndex: 'id',
        align: "center",
    },
    {
        title: 'Name',
        align: "center",
        render: rec => {
            if(isEditing && selectedRow === rec["id"]){
                return <Input 
                            value={rec["name"]}
                            onChange={onChangeInput("name")}
                        />
            }else{
                return rec["name"]
            }
        }
    },
    {
        title: 'Capital',
        align: "center",
        render: rec => {
            if(isEditing && selectedRow === rec["id"]){
                return <Input 
                            onChange={onChangeInput("capital")}
                            value={rec["capital"]}
                        />
            }else{
                return rec["capital"]
            }
        }
    },
    {
        title: 'Flag',
        align: "center",
        render: rec => {
            if(isEditing && selectedRow === rec["id"]){
                return <Input type="file" onChange={onChangeInput("capital")}/>
            }else{
                return <Image src={rec["flag"]} height={40} width={55} />
            }
        }
    },
    {
        title: 'Region',
        align: "center",
        render: rec => {
            if(isEditing && selectedRow === rec["id"]){
                return <Input 
                            onChange={onChangeInput("region")}
                            value={rec["region"]}
                        />
            }else{
                return rec["region"]
            }
        }
    },
    {
        title: 'Population',
        align: "center",
        render: rec => {
            if(isEditing && selectedRow === rec["id"]){
                return <Input 
                            onChange={onChangeInput("population")}
                            value={rec["population"]}
                        />
            }else{
                return rec["population"]
            }
        }
    },
    {
        title: 'Position',
        align: "center",
        render : rec => {
            return (
                <a href={`https://www.google.com/maps/place/@${rec["latlng"][0]},${rec["latlng"][1]},15z`} >
                    <img src={GoogleMapImg} height={30} width={35}/>       
                </a>
            )
        }
    },
    {
        title: 'Actions',
        align: "center",
        render : rec => <div className="site-button-ghost-wrapper">
            <Button type="ghost" onClick={onRowSelect(rec["id"])}>Edit</Button>
            <Button type="primary" onClick={editRecord}>Save</Button>
        </div>
    },

]

export const initialRowData = {
    name:"",
    capital:"",
    latlng:"",
    population:"",
    region:"",
    flag:"",
    isNew:true
}