import { takeLatest, put, all, select , call } from "redux-saga/effects";
import {
    addNewRecordFinished,
    editRecordFinished,
    getCountriesFinished,
} from "./action";
import {
    GET_COUNTRIES,
    ADD_NEW_RECORD,
    EDIT_RECORD,
} from "./type";
import axios from 'axios'
import normalize from "./normalize";
// import notification from "antd/lib/notification";


function* requestCountries() {
  try {
    let response = yield call(() => axios.get("https://restcountries.eu/rest/v2/all"))
    yield put(getCountriesFinished(normalize(response.data)))
  } catch (error) {
    console.log(error.response);
  }
}


// for table Editing
function* requestAddCountry(){
    try {
      const { updatedRow } = yield select( ({ countryReducer }) => countryReducer)
      console.log("form", updatedRow)
      const formData = new FormData()
      Object.keys(updatedRow).forEach(key => formData.append(key , updatedRow[key]))
      let response = yield call(() => axios.post(`https://restcountries.eu/rest/v2/all/${updatedRow.id}` , formData , {
        headers: {"content-type": "multipart/form-data"}
      }))
      console.log(response)
        yield put(addNewRecordFinished({
          isEditing:false
        }))
    } catch (error) {
        console.log("error while get ds =>", error);
    }
}

function* requestUpdateCountry(){
  try {
    const { updatedRow } = yield select( ({ countryReducer }) => countryReducer)
    console.log("form", updatedRow)
    const formData = new FormData()
    Object.keys(updatedRow).forEach(key => formData.append(key , updatedRow[key]))
    let response = yield call(() => axios.put("https://restcountries.eu/rest/v2/all" , formData , {
      headers: {"content-type": "multipart/form-data"}
    }))
    console.log(response)
      yield put(editRecordFinished({
          isEditing:false
        }))
  } catch (error) {
      console.log("error while get ds =>", error);
  }
}


export default function* countrySaga() {
  yield all([takeLatest(GET_COUNTRIES, requestCountries)]);

  /// for Table Editing
  yield all([takeLatest(ADD_NEW_RECORD, requestAddCountry)]);
  yield all([takeLatest(EDIT_RECORD, requestUpdateCountry)]);

}
